(function() {
    return {
        events: {
            'app.activated': 'init',
            'click #orangestor-refresh': 'fetchfiles'
        },
        requests: {
            fetchfiles: function() {
                return {
                    url: 'https://www.orangestor.com/api/bundle/agent/' + this.setting('token') + '/' + this.ticket().id(),
                    type: 'GET'
                };
            }
        },
        init: function() {
            this.fetchfilesajax();
        },
        fetchfiles: function(event) {
            this.fetchfilesajax();
        },
        fetchfilesajax: function() {
            var self = this;
            self.switchTo('main', {
                files: [],
                showspinner: true
            });
            self.ajax('fetchfiles').done(function(data) {
                if (data) {
                    data = data.reverse();
                } else {
                    data = [];
                }
                self.switchTo('main', {
                    files: data,
                    showspinner: false,
                });
            }).fail(function() {
                self.switchTo('main', {
                    files: [],
                    showspinner: false,
                });
            });
        }
    };
}());
